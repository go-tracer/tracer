package tracer

import (
	"flag"
	"fmt"
	es "gitlab.com/go-tracer/sdk/elastic"
)

var (
	TracerIns      *Tracer
	elkConnections = flag.String("elk connections string", "http://localhost:9200", "elk connections string")
)

type Tracer struct {
	Config Config
}

type Config struct {
	DBConnect  *[]string
	ELKConnect *[]string
	Mode       *string
}

type Trace struct {
	RequestId    string      `json:"requestId"`
	ServiceName  string      `json:"serviceName"`
	Data         interface{} `json:"data"`
	Successfully interface{} `json:"successfully"`
}

func InitTracer(config Config) {
	TracerIns = &Tracer{Config: config}
}

//func (c *Config) Apply(tracer Tracer) {
//	tracer.TracerConfig.DBConnect = c.DBConnect
//	tracer.TracerConfig.ELKConnect = c.ELKConnect
//	tracer.TracerConfig.Mode = c.Mode
//}

func (t *Tracer) SaveTrace(trace Trace) error {
	conn := []string{}
	conn = append(conn, *elkConnections)
	err := es.CreateIndex(conn, trace, trace.RequestId)
	if err != nil {
		return err
	}
	fmt.Printf("write mode: %s\n", *TracerIns.Config.Mode)
	fmt.Println(trace)
	return nil
}
