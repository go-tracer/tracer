module gitlab.com/go-tracer/tracer

go 1.20

require gitlab.com/go-tracer/sdk v0.0.0-20230914063758-62180dbf561d

require github.com/elastic/go-elasticsearch/v7 v7.17.10 // indirect
